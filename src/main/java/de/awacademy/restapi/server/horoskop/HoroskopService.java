package de.awacademy.restapi.server.horoskop;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class HoroskopService {

    private List<String> messages = new ArrayList<>();

    public HoroskopService() {
        this.messages.addAll(Arrays.asList(
                "Alles bestens, keine Fehler oder Warnungen.",
                "Ihnen kann heute kein Unit Test fehlschlagen.",
                "Ihre klare Sicht auf den Code ist weiterhin ungetrübt.",
                "Sie lassen sich auch vom Compiler nicht die Freude verderben.",
                "Sie stehen über den Dingen, wie eine Oberklasse.",
                "Code Reviews verlaufen zu Ihren Gunsten.",
                "Sie haben den Überblick über die Klassen-Hierarchie.",
                "Sie haben ein unglaubliches Verständnis für Objektorientierung heute.",
                "Refactoring ist wichtig, im Code wie auch bei den persönlichen Gewohnheiten.",
                "Heute fangen Sie alle Exceptions.",
                "Sie wissen, was Sie wollen, und implementieren das auch.",
                "Der Mond steht gut. Neue Parameter erscheinen ab 11 Uhr.",
                "Nutzen Sie die Möglichkeit, um Instanzvariablen zu lesen.",
                "Die Sonne hat nun das Vorzeichen gewechselt. Sie versorgt Sie mit neuer Tatkraft.",
                "Die Venus steht im Zeichen der ArrayList.",
                "Der Tag verläuft ohne Merge-Konflikte.",
                "Heute ist ein guter Tag für neue User Stories.",
                "Ihre Commit-Messages sind heute erfüllt von Freude."
        ));
    }

    public Horoskop berechneHoroskop(LocalDate geburtsDatum) {
        LocalDate today = LocalDate.now();
        long seed = geburtsDatum.getYear() + geburtsDatum.getMonthValue() * 100 + geburtsDatum.getDayOfMonth() * 10000
                + today.toEpochDay();

        Random r = new Random(seed);

        return new Horoskop(
                messages.get(r.nextInt(messages.size())),
                r.nextInt(60) + 40,
                r.nextInt(60) + 40,
                r.nextInt(60) + 40
        );

    }
}
