package de.awacademy.restapi.server.horoskop;

public class Horoskop {

    private String message;
    private int energie;
    private int karma;
    private int freude;

    public Horoskop(String message, int energie, int karma, int freude) {
        this.message = message;
        this.energie = energie;
        this.karma = karma;
        this.freude = freude;
    }

    public String getMessage() {
        return message;
    }

    public int getEnergie() {
        return energie;
    }

    public int getKarma() {
        return karma;
    }

    public int getFreude() {
        return freude;
    }
}
